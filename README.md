# CollegeProjects
This is a repository to store my college projects.  I'm transferring the projects here and transcribing them into LaTeX, but I'm not appreciably changing the text contained in them.

# Distributed Robotic Mapping System

The goal of this project was to design, build, and construct a robotic chassis that could be easily manufactured to carry several sensors.  These sensors would track the robot's location, detect obstacles, and transmit data regarding its location and the obstacles encountered to a central computer for collection and merging.

The mapping and tracking systems were developed by fellow students and I was tasked with creating the robot itself.

# MAE 3191 - Winching Vehicle

Provided a motor, design and construct a vehicle that can carry a 1kg mass up a 45 degree incline and stop at the top without using electronic timing methods.

There will be a total of three runs and faster times will be encouraged.

# MAE 3187 - Numerical Solution of a Two-Dimensional, Transient Temperature Distribution in a Rectangular Domain

An aluminum plate is being cooled along two adjecent sides with chilled water.
The two other sides are are being heated with strip heaters as shown in the figure below.
Initially, the aluminum is at room temperature before these boundary conditionas are imposed.
As the plate has thermal inertia, the temperature distribution in the plate will change with time and eventually reach steady state.

Find the temperature distribution after 90 seconds, 360 seconds, and at steady state.

# MAE 3195 - Forced-Entry Ballistic (FEBR) Louver Design

Construct a 3D parametric, feature-based solid model assembly of the Norshield FEBR louver and sorm louver, with geometry that mimics what is shown.
A storm louver is typically plaed in front of the FEBR louver to keep out weather and to shut the opening as required.
This louver is an active device that can be remotely opened or closed on demand.
Using a geometry of your choosing, you must design a storm louver that closes off the opening while minimizing the obstruction of the free area when in the open position.

Analyze the FEBR louver using FEA and the following design criteria: a static pressure distribution of 1000psi,  a sinusoidal pressure distribution with a maximum pressure of 1000psi, and a static pressure distribution corresponding to the impac of a 9mm handgun bullet.

Perform an optimization or a sensitivity study to select the best dimensions for the FEBR louver.
