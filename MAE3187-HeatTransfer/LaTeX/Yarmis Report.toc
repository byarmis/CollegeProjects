\contentsline {section}{Introduction}{2}
\contentsline {section}{Results}{3}
\contentsline {subsection}{30 Nodes, No Heat Generation}{3}
\contentsline {subsubsection}{90 Seconds}{3}
\contentsline {subsubsection}{360 Seconds}{4}
\contentsline {subsection}{60 Nodes, No Heat Generation}{5}
\contentsline {subsubsection}{90 Seconds}{5}
\contentsline {subsubsection}{360 Seconds}{6}
\contentsline {subsection}{30 Nodes, 1 $\sfrac {W}{cm^3}$ Heat Generation}{7}
\contentsline {subsubsection}{90 Seconds}{7}
\contentsline {subsubsection}{360 Seconds}{8}
\contentsline {subsection}{60 Nodes, 1 $\sfrac {W}{cm^3}$ Heat Generation}{9}
\contentsline {subsubsection}{90 Seconds}{9}
\contentsline {subsubsection}{360 Seconds}{10}
\contentsline {subsection}{Overview}{11}
\contentsline {subsection}{Steady-State}{11}
\contentsline {subsubsection}{Calculations}{11}
\contentsline {subsection}{Numerical Instability}{12}
\contentsline {section}{Code}{13}
\contentsline {subsection}{Main Program}{13}
\contentsline {subsection}{Steady-State Calculation}{16}
\contentsline {subsection}{Functions}{17}
\contentsline {subsubsection}{\texttt {converge}}{17}
\contentsline {subsubsection}{\texttt {plotq}}{19}
\contentsline {subsubsection}{\texttt {tbedge}}{20}
\contentsline {subsubsection}{\texttt {tcorner}}{21}
\contentsline {subsubsection}{\texttt {tledge}}{22}
\contentsline {subsubsection}{\texttt {tnode}}{23}
